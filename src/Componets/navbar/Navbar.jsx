import React from 'react'
import { Link } from "react-router-dom";
import "../../Assets/style/style.css";
import Logo from '../../Assets/images/logo/maketimely-logo.svg';
//import { Navbar, Dropdown, Nav, Link, NavbarBrand, Nav, Toggle, Collapse, Item, Divider } from 'react-bootstrap';
//import  * as Navbar from 'react-bootstrap';


const Navbar = () => {
  return (
    
    <div className="Container-fluid nav_bg" >
      <div className="row">
        <div className="col-10 mx-auto">
         <div className='navbar__container'>
          <nav className="navbar navbar-expand-lg navbar-light">
            <div className="container-fluid nav-bg">
            
              <Link exact className="navbar-brand" to="/">
              <img src={Logo} alt="Mt logo" />
              </Link>
               <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                 <span className="navbar-toggler-icon"></span>
               </button>
                <div className="collapse navbar-collapse" id="navbarNavDropdown">
                <ul className="navbar-nav ml-auto mb-2 mb-lg-0">
                    <li className="nav-item">
                     <Link exact className="nav-link" to="/explore">Explore</Link>
                    </li>
                    <li class="nav-item dropdown">
                     <Link exact className="nav-link dropdown-toggle" to="/" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                       Programs
                     </Link>
                     <ul className="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                       <li><Link exact className="dropdown-item" to="/programs">Full Stack Developer</Link></li>
                       <li><Link exact className="dropdown-item" to="/">Backend Developer</Link></li>
                       <li><Link exact className="dropdown-item" to="/">Front-End Developer</Link></li>
                     </ul>
                    </li>
        
                    <li class="nav-item">
                     <Link exact className="nav-link" to="/">Help</Link>
                    </li>
        <li class="nav-item">
          <Link exact className="nav-link" to="/">Sign In</Link>
        </li>
        <li className="nav-item ">
          <Link exact className="nav-link " to="/"><span className="rcorners2">Sign Up</span></Link>
        </li>
      </ul>
    </div>
  </div>
  
   </nav>
    </div>
    </div>
      </div>
    </div>
    
  )
}

export default Navbar;