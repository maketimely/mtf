import React from 'react';
import { Link, BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import Course from './pages/Course';
import Explore from './pages/Explore';
import Programs from './pages/programs/Programs';
import Navbar from "./Componets/navbar/Navbar";


const App = () => {
  return (
    <>
    <BrowserRouter>
    <Navbar />
     <Switch>
       <Route exact path="/" component={Course} />
       <Route exact path="/programs" component={Programs} />
       <Route exact path="/explore" component={Explore} />
       <Redirect to="/" />
     </Switch>
    </BrowserRouter>
   </>
  );
};

export default App;


